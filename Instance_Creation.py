import math
import pickle
import time
from datetime import datetime
from multiprocessing import Pool, cpu_count

import numpy as np
import scipy.interpolate
import xlrd
from geopy.distance import geodesic

MIN_TRIP_TIME = 20 * 60  # 20 minutes equal to 1200 seconds
MAX_DELTA_TIME = 3
MAX_TRIP_DISTANCE = 500
MIN_GPS_READING = 3
THRESHOLD = 20  # fixed of number of GPS points for each instance
# Identify the speed and acceleration limit
# SPEED_LIMIT = {0: 7, 1: 12, 2: 120./3.6, 3: 180./3.6, 4: 120/3.6}
SPEED_LIMIT = {0: 7, 1: 180.0 / 3.6, 2: 180.0 / 3.6}
# Online sources for acc: walk: 1.5 Train 1.15, bus. 1.25 (.2), bike: 2.6, train:1.5
# ACC_LIMIT = {0: 3, 1: 3, 2: 2, 3: 10, 4: 3}
ACC_LIMIT = {0: 3, 1: 10, 2: 10}
N_CLASSES = 3

with open("./outputs/Revised_Trajectory_Label_Array.pickle", "rb") as f:
    trajectory_label_array = pickle.load(f)


def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    r"""Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techniques.
    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
    deriv: int
        the order of the derivative to compute (default = 0 means only smoothing)
    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).
    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.
    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()
    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and differentiation of
       data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688
    """
    from math import factorial

    import numpy as np

    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order + 1)
    half_window = (window_size - 1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window + 1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs(y[1 : half_window + 1][::-1] - y[0])
    lastvals = y[-1] + np.abs(y[-half_window - 1 : -1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve(m[::-1], y, mode="valid")


def calculate_delta_time(data):
    delta_time = []
    for i in range(len(data) - 1):
        # Agregamos a delta_time la diferencia entre los timestamps de cada entrada, en segundos
        delta_time.append((data[i + 1, 2] - data[i, 2]) * 24.0 * 3600)
        if delta_time[i] == 0:
            # Prevent to generate infinite speed. So use a very short time = 0.1 seconds.
            delta_time[i] = 0.1
        # Since there is no data for the last point, we assume the delta_time as the average time in the user guide
    # (i.e., 3 sec) and speed as temp_speed equal to last time so far.
    delta_time.append(3)
    return delta_time


def filter_instances(data, delta_time):
    # instance_number: Break a user's trajectory to instances. Count number of GPS points for each instance
    # instance_number: indicate the length of each instance
    instance_number = []
    # label: For each created instance, we need only one mode to be assigned to.
    # Remove the instance with less than 10 GPS points. Break the whole user's trajectory into trips with min_trip
    # Also break the instance with more than THRESHOLD GPS points into more instances
    data_all_instance = []  # Each of its element is a list that shows the data for each instance (lat, long, time)
    label = []
    avgs_sum = np.zeros(N_CLASSES)
    avgs_count = np.zeros(N_CLASSES)
    lost = np.zeros(N_CLASSES)
    i = 0
    while i <= (len(data) - 1):
        mode_type = data[i, 3]
        counter = 0
        # index: save the instance indices when an instance is being created and concatenate all in the remove
        index = []
        A = (data[i, 0], data[i, 1])
        # First, we always have an instance with one GPS point.
        while i <= (len(data) - 1) and data[i, 3] == mode_type and counter < THRESHOLD:
            B = (data[i, 0], data[i, 1])
            distance = calculate_relative_distance(A, B)

            if distance > MAX_TRIP_DISTANCE or delta_time[i] > MIN_TRIP_TIME:
                i += 1
                break

            if delta_time[i] <= MAX_DELTA_TIME:
                index.append(i)
                counter += 1

            i += 1

        if counter >= MIN_GPS_READING:  # Remove all instances that have less than MIN_GPS_READING GPS points
            avgs_sum[int(mode_type)] = avgs_sum[int(mode_type)] + counter
            avgs_count[int(mode_type)] = avgs_count[int(mode_type)] + 1

            instance_number.append(counter)
            data_for_instance = [data[i, 0:3] for i in index]
            data_for_instance = np.array(data_for_instance, dtype=float)
            data_all_instance.append(data_for_instance)
            label.append(mode_type)
        else:
            lost[int(mode_type)] = lost[int(mode_type)] + 1
    return instance_number, data_all_instance, label, avgs_sum, avgs_count, lost


def calculate_bearing(first_point_coordinates, second_point_coordinates):
    first_point_longitude = first_point_coordinates[0]
    first_point_latitude = first_point_coordinates[1]
    second_point_longitude = second_point_coordinates[0]
    second_point_latitude = second_point_coordinates[1]
    y = math.sin(math.radians(second_point_latitude) - math.radians(first_point_latitude)) * math.radians(
        math.cos(second_point_longitude)
    )
    x = math.radians(math.cos(first_point_longitude)) * math.radians(math.sin(second_point_longitude)) - math.radians(
        math.sin(first_point_longitude)
    ) * math.radians(math.cos(second_point_longitude)) * math.radians(
        math.cos(second_point_latitude) - math.radians(first_point_latitude)
    )
    # Convert radian from -pi to pi to [0, 360] degree
    b = (math.atan2(y, x) * 180.0 / math.pi + 360) % 360
    return b


def calculate_relative_distance(first_point_coordinates, second_point_coordinates):
    if second_point_coordinates[0] > 90:
        buf = second_point_coordinates
        second_point_coordinates = (buf[0] / 100, buf[1])
    if first_point_coordinates[0] > 90:
        buf = first_point_coordinates
        first_point_coordinates = (buf[0] / 100, buf[1])
    return geodesic(first_point_coordinates, second_point_coordinates).meters


def calculate_speed_outliers(speed, speed_limit):
    speed_outlier = []
    for i in range(len(speed)):
        if speed[i] > speed_limit or speed[i] < 0:
            speed_outlier.append(i)
    return speed_outlier


def remove_outliers(outliers_indexes, *args):
    outliers_indexes = sorted(outliers_indexes, reverse=True)
    for feature_array in args:
        for index in outliers_indexes:
            del feature_array[index]


def calculate_acc_outliers(accelerations, acc_limit):
    acc_outlier = []
    for i in range(len(accelerations)):
        if abs(accelerations[i]) > acc_limit:
            acc_outlier.append(i)
    return acc_outlier


def calculate_accelerations(speeds, delta_times):
    ret = []
    for i in range(len(speeds) - 1):
        delta_speed = speeds[i + 1] - speeds[i]
        acc = delta_speed / delta_times[i]
        ret.append(acc)
    return ret


def calculate_jerk(accelerations, delta_times):
    ret = []
    for i in range(len(accelerations) - 1):
        diff = accelerations[i + 1] - accelerations[i]
        j = diff / delta_times[i]
        ret.append(j)
    return ret


def calculate_bearing_rate(bearings):
    ret = []
    for i in range(len(bearings) - 1):
        diff = abs(bearings[i + 1] - bearings[i])
        ret.append(diff)
    return ret


def calculate_velocity_change(speeds):
    ret = []
    for i in range(len(speeds) - 1):
        diff = abs(speeds[i + 1] - speeds[i])
        if speeds[i] != 0:
            ret.append(diff / speeds[i])
        else:
            ret.append(1)
    return ret


def process_trajectory_label_array(trajectory_label_array, pool_index):
    ret_total_relative_distance = []
    ret_total_speed = []
    ret_total_acceleration = []
    ret_total_jerk = []
    ret_total_bearing_rate = []
    ret_total_delta_time = []
    ret_total_velocity_change = []
    ret_total_label = []
    ret_total_instance_number = []
    ret_total_outlier = []
    ret_total_instance_in_sequence = []
    ret_dayOfWeek = []
    ret_hourOfDay = []
    ret_lat = []
    ret_long = []

    ret_avgs_sum = np.zeros(N_CLASSES)
    ret_avgs_count = np.zeros(N_CLASSES)
    ret_lost = np.zeros(N_CLASSES)

    outliers_amount = 0
    for z in range(len(trajectory_label_array)):

        # data tiene la forma: [[lat,long,timestamp,índice de label]
        #                       [lat,long,timestamp,índice de label]
        #                       ...
        #                       [lat,long,timestamp,índice de label]]
        data = trajectory_label_array[z]

        (
            result,
            relative_distance,
            speed,
            acceleration,
            jerk,
            bearing_rate,
            delta_time,
            velocity_change,
            label,
            instance_number,
            user_outlier,
            avgs_sum,
            avgs_count,
            lost,
            dayOfWeek,
            hourOfDay,
            lat,
            long,
        ) = process_trajectory_label(data)

        if result:
            ret_total_relative_distance.append(relative_distance)
            ret_total_speed.append(speed)
            ret_total_acceleration.append(acceleration)
            ret_total_jerk.append(jerk)
            ret_total_bearing_rate.append(bearing_rate)
            ret_total_delta_time.append(delta_time)
            ret_total_velocity_change.append(velocity_change)
            ret_total_label.append(label)
            ret_total_instance_number.append(instance_number)
            ret_total_outlier.append(user_outlier)
            ret_total_instance_in_sequence = ret_total_instance_in_sequence + instance_number
            ret_dayOfWeek.append(dayOfWeek)
            ret_hourOfDay.append(hourOfDay)
            ret_lat.append(lat)
            ret_long.append(long)
            ret_avgs_sum = ret_avgs_sum + avgs_sum
            ret_avgs_count = ret_avgs_count + avgs_count
            ret_lost = ret_lost + lost

    return (
        pool_index,
        ret_total_relative_distance,
        ret_total_speed,
        ret_total_acceleration,
        ret_total_jerk,
        ret_total_bearing_rate,
        ret_total_delta_time,
        ret_total_velocity_change,
        ret_total_label,
        ret_total_instance_number,
        ret_total_outlier,
        ret_total_instance_in_sequence,
        outliers_amount,
        ret_avgs_sum,
        ret_avgs_count,
        ret_lost,
        ret_dayOfWeek,
        ret_hourOfDay,
        ret_lat,
        ret_long,
    )


def distribute_interpolate(relative_distance, input):
    x = np.zeros(THRESHOLD)
    sums = np.zeros(THRESHOLD)
    counts = np.zeros(THRESHOLD)
    total_distances = np.sum(relative_distance)
    acu_distance = 0
    counts = counts + 1

    if total_distances == 0:
        return input

    for i in range(0, len(input)):
        acu_distance = acu_distance + relative_distance[i]
        new_pos = int((THRESHOLD - 1) * acu_distance / total_distances)
        sums[new_pos] = sums[new_pos] + input[i]
        counts[new_pos] = counts[new_pos] + 1
        x[new_pos] = new_pos

    final = sums / counts
    x[THRESHOLD - 1] = THRESHOLD - 1
    y_interp = scipy.interpolate.interp1d(x, final, copy=False)
    salida = y_interp([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19])

    return salida


def process_trajectory_label(data):
    outliers_amount = 0

    if len(data) == 0:
        return False, [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []

    delta_time = calculate_delta_time(data)

    instance_number, data_all_instance, label, ret_avgs_sum, ret_avgs_count, ret_lost = filter_instances(
        data, delta_time
    )

    if len(instance_number) == 0:
        return False, [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []

    label = [int(i) for i in label]

    relative_distance = [[] for _ in range(len(instance_number))]
    speed = [[] for _ in range(len(instance_number))]
    acceleration = [[] for _ in range(len(instance_number))]
    jerk = [[] for _ in range(len(instance_number))]
    bearing = [[] for _ in range(len(instance_number))]
    bearing_rate = [[] for _ in range(len(instance_number))]
    delta_time = [[] for _ in range(len(instance_number))]
    velocity_change = [[] for _ in range(len(instance_number))]
    dayOfWeek = [[] for _ in range(len(instance_number))]
    hourOfDay = [[] for _ in range(len(instance_number))]
    lat = [[] for _ in range(len(instance_number))]
    long = [[] for _ in range(len(instance_number))]
    user_outlier = []

    # Create channels for every instance (k) of the current user
    for k in range(len(instance_number)):
        # Each element of data_all_instance is a list that shows the data for each instance (lat, long, time)
        data = data_all_instance[k]

        for i in range(len(data) - 1):
            first_point_coordinates = (data[i, 0], data[i, 1])
            second_point_coordinates = (data[i + 1, 0], data[i + 1, 1])
            rel_distance = calculate_relative_distance(first_point_coordinates, second_point_coordinates)
            relative_distance[k].append(rel_distance)
            delta_time[k].append((data[i + 1, 2] - data[i, 2]) * 24.0 * 3600 + 1)  # Add one second to prevent zero time
            instance_speed = rel_distance / delta_time[k][i]
            speed[k].append(instance_speed)
            b = calculate_bearing(first_point_coordinates, second_point_coordinates)
            bearing[k].append(b)

            python_date = datetime(*xlrd.xldate_as_tuple(data[i, 2], 0))
            dayOfWeek[k].append(python_date.weekday())
            hourOfDay[k].append(python_date.hour)
            lat[k].append(data[i, 0])
            long[k].append(data[i, 1])

        speed_outliers = calculate_speed_outliers(speed[k], SPEED_LIMIT[label[k]])

        # Remove all outliers (exceeding max speed) in the current instance
        remove_outliers(speed_outliers, speed[k])
        if len(speed[k]) < MIN_GPS_READING:
            instance_number[k] = 0
            outliers_amount += 1
            continue
        # duplica el ultimo elemento de la lista ( esto será por que no hay datos para el último punto como comenta más arriba?)
        # y si es así: por qué tenemos este punto sin datos, son impares?. Por qué no restar 1 y elegir crear uno de esta manera?
        speed[k].append(speed[k][-1])
        relative_distance[k].append(relative_distance[k][-1])
        delta_time[k].append(delta_time[k][-1])
        bearing[k].append(bearing[k][-1])

        # Remove corresponding points from other channels.
        remove_outliers(
            speed_outliers, relative_distance[k], bearing[k], delta_time[k], dayOfWeek[k], hourOfDay[k], lat[k], long[k]
        )

        acceleration[k] = calculate_accelerations(speed[k], delta_time[k])
        acc_outliers = calculate_acc_outliers(acceleration[k], ACC_LIMIT[label[k]])

        # Remove all outlier instances, where their acceleration exceeds the max acceleration.
        remove_outliers(acc_outliers, acceleration[k])
        if len(acceleration[k]) < MIN_GPS_READING:
            instance_number[k] = 0
            outliers_amount += 1
            continue
        acceleration[k].append(acceleration[k][-1])

        # Remove corresponding points from other channels.
        remove_outliers(
            acc_outliers,
            speed[k],
            relative_distance[k],
            bearing[k],
            delta_time[k],
            dayOfWeek[k],
            hourOfDay[k],
            lat[k],
            long[k],
        )
        jerk[k] = calculate_jerk(acceleration[k], delta_time[k])

        if len(jerk[k]) < MIN_GPS_READING:
            instance_number[k] = 0
            outliers_amount += 1
            continue
        jerk[k].append(jerk[k][-1])

        # Update instance_number after outlier removal
        instance_number[k] = instance_number[k] - len(speed_outliers) - len(acc_outliers)

        # Compute bearing_rate from bearing, and velocity_change from speed
        bearing_rate[k] = calculate_bearing_rate(bearing[k])
        bearing_rate[k].append(bearing_rate[k][-1])

        velocity_change[k] = calculate_velocity_change(speed[k])
        velocity_change[k].append(velocity_change[k][-1])

        # Smoothing filter on each instance:
        if instance_number[k] < THRESHOLD:
            relative_distance[k] = distribute_interpolate(relative_distance[k], relative_distance[k])
            speed[k] = distribute_interpolate(relative_distance[k], speed[k])
            acceleration[k] = distribute_interpolate(relative_distance[k], acceleration[k])
            jerk[k] = distribute_interpolate(relative_distance[k], jerk[k])
            bearing_rate[k] = distribute_interpolate(relative_distance[k], bearing_rate[k])

        relative_distance[k] = savitzky_golay(np.array(relative_distance[k]), THRESHOLD - 1, 3)
        speed[k] = savitzky_golay(np.array(speed[k]), THRESHOLD - 1, 3)
        acceleration[k] = savitzky_golay(np.array(acceleration[k]), THRESHOLD - 1, 3)
        jerk[k] = savitzky_golay(np.array(jerk[k]), THRESHOLD - 1, 3)
        bearing_rate[k] = savitzky_golay(np.array(bearing_rate[k]), THRESHOLD - 1, 3)

    return (
        True,
        relative_distance,
        speed,
        acceleration,
        jerk,
        bearing_rate,
        delta_time,
        velocity_change,
        label,
        instance_number,
        user_outlier,
        ret_avgs_sum,
        ret_avgs_count,
        ret_lost,
        dayOfWeek,
        hourOfDay,
        lat,
        long,
    )


def main():
    # total_instance_in_sequence checks the number of GPS points for each instance in all users
    total_instance_in_sequence = []
    # total_Motion_instance: each element is an array include the four channels for each instance
    # total_Motion_instance = []
    # Save the 4 channels for each user separately
    total_relative_distance = []
    total_speed = []
    total_acceleration = []
    total_jerk = []
    total_bearing_rate = []
    total_label = []
    total_instance_number = []
    total_outlier = []
    total_delta_time = []
    total_velocity_change = []
    total_dayOfWeek = []
    total_hourOfDay = []
    total_lat = []
    total_long = []
    total_avgs_sum = np.zeros(N_CLASSES)
    total_avgs_count = np.zeros(N_CLASSES)
    total_lost = np.zeros(N_CLASSES)
    # Count the number of times that number_of_outliers happens
    number_of_outliers = 0

    start_time = time.perf_counter()
    cores = cpu_count()
    # print(f'Utilizando {cores} cores')
    # Divide el array a procesar según cantidad de cores
    split_trajectory = np.array_split(trajectory_label_array, cores)

    # Inicia un pool con la cantidad de procesadores que devuelva os.cpu_count()
    pool = Pool()

    # Pasamos parte del input a cada proceso, y un índice para ordenar los resultados al terminar
    # zip(split_trajectory,range(0,cores)) = (split_trajectory[0],0),(split_trajectory[1],1),(split_trajectory[2],2) ...
    results = pool.starmap(process_trajectory_label_array, zip(split_trajectory, range(0, cores)))

    pool.close()
    pool.join()

    # Ordenamos los resultados según pool_index
    results.sort(key=lambda x: x[0])

    for r in results:
        (
            index,
            relative_distance,
            speed,
            acceleration,
            jerk,
            bearing_rate,
            delta_time,
            velocity_change,
            label,
            instance_number,
            user_outlier,
            instance_in_sequence,
            outliers_amount,
            avgs_sum,
            avgs_count,
            lost,
            dayOfWeek,
            hourOfDay,
            lat,
            long,
        ) = r
        if len(instance_in_sequence) > 0:
            total_relative_distance.extend(relative_distance)
            total_speed.extend(speed)
            total_acceleration.extend(acceleration)
            total_jerk.extend(jerk)
            total_bearing_rate.extend(bearing_rate)
            total_delta_time.extend(delta_time)
            total_velocity_change.extend(velocity_change)
            total_label.extend(label)
            total_instance_number.extend(instance_number)
            total_outlier.extend(user_outlier)
            total_instance_in_sequence.extend(instance_in_sequence)
            number_of_outliers += outliers_amount
            total_avgs_sum = np.add(total_avgs_sum, avgs_sum)
            total_avgs_count = np.add(total_avgs_count, avgs_count)
            total_lost = np.add(total_lost, lost)
            total_dayOfWeek.extend(dayOfWeek)
            total_hourOfDay.extend(hourOfDay)
            total_lat.extend(lat)
            total_long.extend(long)

    total_avgs = np.zeros(N_CLASSES)
    for i in range(N_CLASSES):
        total_avgs[i] = total_avgs_sum[i] / total_avgs_count[i]

    # print("cantidades: ", total_avgs_count)
    # print("perdidas: ", total_lost)
    # print("promedios: ", total_avgs)

    with open("./outputs/Revised_InstanceCreation+NoJerkOutlier+Smoothing.pickle", "wb") as f:
        pickle.dump(
            [
                total_relative_distance,
                total_speed,
                total_acceleration,
                total_jerk,
                total_bearing_rate,
                total_label,
                total_instance_number,
                total_instance_in_sequence,
                total_delta_time,
                total_velocity_change,
                total_dayOfWeek,
                total_hourOfDay,
                total_lat,
                total_long,
            ],
            f,
        )
    print("Running time", time.perf_counter() - start_time)


if __name__ == "__main__":
    main()
