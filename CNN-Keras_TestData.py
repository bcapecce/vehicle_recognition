import pickle

import numpy as np
import tensorflow as tf
from sklearn.metrics import confusion_matrix

tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(allow_soft_placement=True, log_device_placement=True))
sess = print(tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(log_device_placement=True)))


filename = "./outputs/Revised_KerasData_NoSmoothing.pickle"

with open(filename, mode="rb") as f:
    total_input, final_label = pickle.load(f, encoding="latin1")  # Also can use the encoding 'iso-8859-1'

NoClass = len(list(set(np.ndarray.flatten(final_label))))

# load Model
model = tf.keras.models.load_model("vehicle_recognition.model")

# Calculating the test accuracy, precision, recall
Pred = model.predict(total_input, batch_size=32)
Pred_Label = np.argmax(Pred, axis=1)

ActualPositive = []
for i in range(NoClass):
    AA = np.where(final_label == i)[0]
    ActualPositive.append(AA)

PredictedPositive = []
for i in range(NoClass):
    AA = np.where(Pred_Label == i)[0]
    PredictedPositive.append(AA)

TruePositive = []
FalsePositive = []
for i in range(NoClass):
    AA = []
    BB = []
    for j in PredictedPositive[i]:
        if Pred_Label[j] == final_label[j]:
            AA.append(j)
        else:
            BB.append(j)
    TruePositive.append(AA)
    FalsePositive.append(BB)
Precision = []
Recall = []
for i in range(NoClass):
    Precision.append(0 if len(PredictedPositive[i]) == 0 else len(TruePositive[i]) * 1.0 / len(PredictedPositive[i]))
    Recall.append(0 if len(ActualPositive[i]) == 0 else len(TruePositive[i]) * 1.0 / len(ActualPositive[i]))

ConfusionM = confusion_matrix(list(final_label), Pred_Label, labels=[0, 1, 2, 3, 4])

print("Confusion Matrix: \n", ConfusionM)
print("Recall", Recall)
print("precision", Precision)

print("label", final_label)
print("predi", Pred_Label)
