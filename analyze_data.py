import pickle
import time
from multiprocessing import Pool, cpu_count

import numpy as np
from geopy.distance import geodesic

MIN_TRIP_TIME = 20 * 60  # 20 minutes equal to 1200 seconds
MAX_DELTA_TIME = 2
MAX_TRIP_DISTANCE = 100
MIN_GPS_READING = 3
THRESHOLD = 10  # fixed of number of GPS points for each instance

# mode_index = {"walk": 0, "run": 1, "bike": 2, "bus": 3, "car": 4, "taxi": 5, "subway": 6, "railway": 7,
#                  "train": 8, "motocycle": 9, "boat": 10, "airplane": 11, "other": 12}
# filtro 3, todo en 100
SPEED_LIMIT = {
    0: 7,
    1: 14,
    2: 180.0 / 3.6,
    3: 180.0 / 3.6,
    4: 180.0 / 3.6,
    5: 180.0 / 3.6,
    6: 180.0 / 3.6,
    7: 180.0 / 3.6,
    8: 180.0 / 3.6,
    9: 180.0 / 3.6,
    10: 180.0 / 3.6,
    11: 1000.0 / 3.6,
    12: 180.0 / 3.6,
}
# filtro 4, todo en 100
ACC_LIMIT = {0: 3, 1: 6, 2: 10, 3: 10, 4: 10, 5: 10, 6: 10, 7: 10, 8: 10, 9: 10, 10: 10, 11: 10, 12: 10}


# Identify the speed and acceleration limit
# Online sources for acc: walk: 1.5 Train 1.15, bus. 1.25 (.2), bike: 2.6, train:1.5

with open("./outputs/Revised_Trajectory_Label_Array.pickle", "rb") as f:
    trajectory_label_array = pickle.load(f)


def calculate_relative_distance(first_point_coordinates, second_point_coordinates):
    if second_point_coordinates[0] > 90:
        buf = second_point_coordinates
        second_point_coordinates = (buf[0] / 100, buf[1])
    if first_point_coordinates[0] > 90:
        buf = first_point_coordinates
        first_point_coordinates = (buf[0] / 100, buf[1])
    return geodesic(first_point_coordinates, second_point_coordinates).meters


def calculate_delta_time(data):
    delta_time = []
    for i in range(len(data) - 1):
        # Agregamos a delta_time la diferencia entre los timestamps de cada entrada, en segundos
        delta_time.append((data[i + 1, 2] - data[i, 2]) * 24.0 * 3600)
        if delta_time[i] == 0:
            # Prevent to generate infinite speed. So use a very short time = 0.1 seconds.
            delta_time[i] = 0.1
        # Since there is no data for the last point, we assume the delta_time as the average time in the user guide
    # (i.e., 3 sec) and speed as temp_speed equal to last time so far.
    delta_time.append(3)
    return delta_time


def filter_instances(data, delta_time):
    # instance_number: Break a user's trajectory to instances. Count number of GPS points for each instance
    # instance_number: indicate the length of each instance
    instance_number = []
    # label: For each created instance, we need only one mode to be assigned to.
    # Remove the instance with less than 10 GPS points. Break the whole user's trajectory into trips with min_trip
    # Also break the instance with more than THRESHOLD GPS points into more instances
    data_all_instance = []  # Each of its element is a list that shows the data for each instance (lat, long, time)
    label = []
    i = 0
    while i <= (len(data) - 1):
        mode_type = data[i, 3]
        counter = 0
        # index: save the instance indices when an instance is being created and concatenate all in the remove
        index = []
        A = (data[i, 0], data[i, 1])
        # First, we always have an instance with one GPS point.
        while (
            i <= (len(data) - 1) and data[i, 3] == mode_type and counter < THRESHOLD
        ):  # Filtro 2, no se considera el THRESHOLD
            B = (data[i, 0], data[i, 1])
            distance = calculate_relative_distance(A, B)
            if distance > MAX_TRIP_DISTANCE or delta_time[i] > MIN_TRIP_TIME or delta_time[i] > MAX_DELTA_TIME:
                i += 1
                break
            index.append(i)
            counter += 1

            i += 1

        if counter >= MIN_GPS_READING:  # Remove all instances that have less than MIN_GPS_READING GPS points
            instance_number.append(counter)
            data_for_instance = [data[i, 0:3] for i in index]
            data_for_instance = np.array(data_for_instance, dtype=float)
            data_all_instance.append(data_for_instance)
            label.append(mode_type)

    return instance_number, data_all_instance, label


def calculate_speed_outliers(speed, speed_limit):
    speed_outlier = []
    for i in range(len(speed)):
        if speed[i] > speed_limit or speed[i] < 0:
            speed_outlier.append(i)
    return speed_outlier


def remove_outliers(outliers_indexes, *args):
    outliers_indexes = sorted(outliers_indexes, reverse=True)
    for feature_array in args:
        for index in outliers_indexes:
            del feature_array[index]


def calculate_acc_outliers(accelerations, acc_limit):
    acc_outlier = []
    for i in range(len(accelerations)):
        if abs(accelerations[i]) > acc_limit:
            acc_outlier.append(i)
    return acc_outlier


def calculate_accelerations(speeds, delta_times):
    ret = []
    for i in range(len(speeds) - 1):
        delta_speed = speeds[i + 1] - speeds[i]
        acc = delta_speed / delta_times[i]
        ret.append(acc)
    return ret


def process_trajectory_label(data):
    outliers_amount = 0

    if len(data) == 0:  # aca se van 5 tipos
        return False, [], [], []

    delta_time = calculate_delta_time(data)
    instance_number, data_all_instance, label = filter_instances(data, delta_time)

    if len(instance_number) == 0:
        return False, [], [], []

    label = [int(i) for i in label]

    relative_distance = [[] for _ in range(len(instance_number))]
    speed = [[] for _ in range(len(instance_number))]
    acceleration = [[] for _ in range(len(instance_number))]
    delta_time = [[] for _ in range(len(instance_number))]

    # Create channels for every instance (k) of the current user
    for k in range(len(instance_number)):
        # Each element of data_all_instance is a list that shows the data for each instance (lat, long, time)
        data = data_all_instance[k]

        for i in range(len(data) - 1):
            first_point_coordinates = (data[i, 0], data[i, 1])
            second_point_coordinates = (data[i + 1, 0], data[i + 1, 1])
            rel_distance = calculate_relative_distance(first_point_coordinates, second_point_coordinates)
            relative_distance[k].append(rel_distance)
            delta_time[k].append((data[i + 1, 2] - data[i, 2]) * 24.0 * 3600 + 1)  # Add one second to prevent zero time
            instance_speed = rel_distance / delta_time[k][i]
            speed[k].append(instance_speed)

        speed_outliers = calculate_speed_outliers(speed[k], SPEED_LIMIT[label[k]])

        # Remove all outliers (exceeding max speed) in the current instance
        remove_outliers(speed_outliers, speed[k])
        if len(speed[k]) < MIN_GPS_READING:
            instance_number[k] = 0
            outliers_amount += 1
            continue
        # duplica el ultimo elemento de la lista ( esto será por que no hay datos para el último punto como comenta más arriba?)
        # y si es así: por qué tenemos este punto sin datos, son impares?. Por qué no restar 1 y elegir crear uno de esta manera?
        speed[k].append(speed[k][-1])
        relative_distance[k].append(relative_distance[k][-1])
        delta_time[k].append(delta_time[k][-1])

        # Remove corresponding points from other channels.
        remove_outliers(speed_outliers, relative_distance[k], delta_time[k])

        acceleration[k] = calculate_accelerations(speed[k], delta_time[k])
        acc_outliers = calculate_acc_outliers(acceleration[k], ACC_LIMIT[label[k]])

        # Remove all outlier instances, where their acceleration exceeds the max acceleration.
        remove_outliers(acc_outliers, acceleration[k])
        if len(acceleration[k]) < MIN_GPS_READING:
            instance_number[k] = 0
            outliers_amount += 1
            continue
        acceleration[k].append(acceleration[k][-1])

        # Remove corresponding points from other channels.
        remove_outliers(acc_outliers, speed[k], relative_distance[k], delta_time[k])

    return True, label, instance_number, delta_time


def process_trajectory_label_array(trajectory_label_array, pool_index):
    ret_total_label = []
    ret_total_instance_number = []
    ret_total_instance_in_sequence = []
    ret_delta_time = []

    for z in range(len(trajectory_label_array)):

        # data tiene la forma: [[lat,long,timestamp,índice de label]
        #                       [lat,long,timestamp,índice de label]
        #                       ...
        #                       [lat,long,timestamp,índice de label]]
        # y corresponde a una persona (hay 69 data en total)
        data = trajectory_label_array[z]

        result, label, instance_number, delta_time = process_trajectory_label(data)

        if result:
            ret_total_label.append(label)
            ret_total_instance_number.append(instance_number)
            ret_delta_time.append(delta_time)
            ret_total_instance_in_sequence = ret_total_instance_in_sequence + instance_number

    return (pool_index, ret_total_label, ret_total_instance_number, ret_total_instance_in_sequence, ret_delta_time)


def main():
    # total_instance_in_sequence checks the number of GPS points for each instance in all users
    total_instance_in_sequence = []
    total_delta_time = []
    total_label = []
    total_instance_number = []

    start_time = time.perf_counter()
    cores = cpu_count()
    print(f"Utilizando {cores} cores")
    # Divide el array a procesar según cantidad de cores
    split_trajectory = np.array_split(trajectory_label_array, cores)

    # Inicia un pool con la cantidad de procesadores que devuelva os.cpu_count()
    pool = Pool()

    # Pasamos parte del input a cada proceso, y un índice para ordenar los resultados al terminar
    # zip(split_trajectory,range(0,cores)) = (split_trajectory[0],0),(split_trajectory[1],1),(split_trajectory[2],2) ...
    results = pool.starmap(process_trajectory_label_array, zip(split_trajectory, range(0, cores)))

    pool.close()
    pool.join()

    # Ordenamos los resultados según pool_index
    results.sort(key=lambda x: x[0])

    for r in results:
        index, label, instance_number, instance_in_sequence, delta_time = r
        if len(instance_in_sequence) > 0:
            total_label.extend(label)
            total_instance_number.extend(instance_number)
            total_instance_in_sequence.extend(instance_in_sequence)
            total_delta_time.extend(delta_time)

    trayectorias_por_persona = []
    for element in total_instance_number:
        trayectorias_por_persona.append(len(element))

    with open("./outputs/Revised_AnalyzeData.pickle", "wb") as f:
        pickle.dump([total_label, total_instance_number, total_instance_in_sequence, total_delta_time], f)
    print("Running time", time.perf_counter() - start_time)

    print("Running time", time.perf_counter() - start_time)


if __name__ == "__main__":
    main()
