import pickle
import random
import time
from collections import Counter

import numpy as np

filename = "./outputs/Revised_InstanceCreation+NoJerkOutlier+Smoothing.pickle"
THRESHOLD = 20  # the max of number of GPS point in an instance
NUMBER_OF_FEATURES = 8


def main():
    start_time = time.perf_counter()
    # Each of the following variables contain multiple lists, where each list belongs to a user
    with open(filename, "rb") as f:
        (
            total_relative_distance,
            total_speed,
            total_acceleration,
            total_jerk,
            total_bearing_rate,
            total_label,
            total_instance_number,
            total_instance_in_sequence,
            total_delta_time,
            total_velocity_change,
            total_dayOfWeek,
            total_hourOfDay,
            total_lat,
            total_long,
        ) = pickle.load(f, encoding="latin1")

    zero_instance = [i for i, item in enumerate(total_instance_in_sequence) if item == 0]
    number_of_instance = len(total_instance_in_sequence) - len(zero_instance)
    total_input = np.zeros((number_of_instance, 1, THRESHOLD, NUMBER_OF_FEATURES), dtype=float)
    final_label = np.zeros((number_of_instance, 1), dtype=int)
    counter = 0

    np.random.seed(7)
    random.seed(7)

    for k in range(len(total_instance_number)):
        # Create Keras shape with 4 channels for each user
        #  There are 4 channels(in order: RelativeDistance, Speed, Acceleration, BearingRate)
        instance_relative_distance = total_relative_distance[k]
        instance_speed = total_speed[k]
        instance_acceleration = total_acceleration[k]
        instance_jerk = total_jerk[k]
        instance_bearing_rate = total_bearing_rate[k]
        instance_label = total_label[k]
        instance_dayOfWeek = total_dayOfWeek[k]
        instance_hourOfDay = total_hourOfDay[k]
        instance_lat = total_lat[k]
        instance_long = total_long[k]

        # instance_number: the instances and number of GPS points in each instance for each user k
        instance_number = total_instance_number[k]

        for i in range(len(instance_number)):
            end = instance_number[i]
            if end == 0 or sum(instance_relative_distance[i]) == 0:
                continue
            end = len(instance_speed[i])
            total_input[counter, 0, 0:end, 0] = instance_speed[i]
            total_input[counter, 0, 0:end, 1] = instance_acceleration[i]
            total_input[counter, 0, 0:end, 2] = instance_jerk[i]
            total_input[counter, 0, 0:end, 3] = instance_bearing_rate[i]
            total_input[counter, 0, 0 : len(instance_dayOfWeek[i]), 4] = instance_dayOfWeek[i]
            total_input[counter, 0, 0 : len(instance_hourOfDay[i]), 5] = instance_hourOfDay[i]
            total_input[counter, 0, 0 : len(instance_lat[i]), 6] = instance_lat[i]
            total_input[counter, 0, 0 : len(instance_long[i]), 7] = instance_long[i]

            final_label[counter, 0] = instance_label[i]
            counter += 1
    # cada elemento (instance) de total_input contiene 200 arrays. Cada array tiene 4 valores: speed, acceleration, jerk, bearing rate
    total_input = total_input[:counter, :, :, :]
    # final_label es básicamente una lista de labels (en forma de índice, ej: 0 para "walk")
    final_label = final_label[:counter, 0]

    # total_input y final_label tienen la misma longitud (cada elemento de final_label se corresponde con cada instance de total_input)

    with open("./outputs/Revised_KerasData_NoSmoothing.pickle", "wb") as f:  # Python 3: open(..., 'wb')
        pickle.dump([total_input, final_label], f)

    print("Running time", time.perf_counter() - start_time)
    print("-------------")
    print(total_input.shape)
    print(Counter(final_label))
    print("-------------")


if __name__ == "__main__":
    main()
