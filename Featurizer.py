import numpy as np
import tensorflow as tf

from Instance_Creation import process_trajectory_label


def convertToFeatures(data):

    THRESHOLD = 10  # the max of number of GPS point in an instance

    total_input = np.zeros((1, 1, THRESHOLD, 5), dtype=float)

    (
        result,
        relative_distance,
        speed,
        acceleration,
        jerk,
        bearing_rate,
        delta_time,
        velocity_change,
        label,
        instance_number,
        user_outlier,
        ret_avgs_sum,
        ret_avgs_count,
        ret_lost,
    ) = process_trajectory_label(data)

    if result:
        counter = 0
        for i in range(len(instance_number)):
            end = instance_number[i]
            if end == 0 or sum(relative_distance[i]) == 0:
                continue
            total_input[counter, 0, 0:end, 0] = np.array(speed)
            total_input[counter, 0, 0:end, 1] = np.array(acceleration)
            total_input[counter, 0, 0:end, 2] = np.array(jerk)
            total_input[counter, 0, 0:end, 3] = np.array(bearing_rate)
            total_input[counter, 0, 0:end, 4] = np.array(instance_number)

            counter += 1

    return total_input


def main():

    data = [
        [39.921712, 116.472343, 39298.1462037037, 0],
        [39.921705, 116.472343, 39298.1462152778, 0],
        [39.921695, 116.472345, 39298.1462268519, 0],
        [39.921683, 116.472342, 39298.1462384259, 0],
        [39.921672, 116.472342, 39298.14625, 0],
        [39.921583, 116.472315, 39298.1462731481, 0],
        [39.92156, 116.47229, 39298.1462847222, 0],
        [39.921565, 116.47229, 39298.1462962963, 0],
        [39.92157, 116.472288, 39298.1463078704, 0],
        [39.921577, 116.4723, 39298.1463310185, 0],
    ]

    data = np.array(data)

    features = convertToFeatures(data)

    print(np.shape(features))

    features_to_print = features[0][0]
    for i in range(0, 9):
        print(
            "speed:",
            features_to_print[i][0],
            "acceleration:",
            features_to_print[i][1],
            "jerk:",
            features_to_print[i][2],
            "bearing_rate:",
            features_to_print[i][3],
            "gps readings:",
            features_to_print[i][4],
        )

    # load Model
    model = tf.keras.models.load_model("vehicle_recognition.model")
    Pred = model.predict(features, batch_size=32)
    Pred_Label = np.argmax(Pred, axis=1)

    print("prediction label is", Pred_Label)

    return features


if __name__ == "__main__":
    main()
