import datetime
import pickle
import random
import time
from collections import Counter
from random import sample

import numpy as np
import tensorflow as tf
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix
from sklearn.model_selection import train_test_split

NUMBER_OF_FEATURES = 8
EPOCHS = 30
BATCH_SIZE = 64
THRESHOLD = 20  # the max of number of GPS point in an instance
list_of_indexes_to_add = []
np.random.seed(7)
random.seed(7)


def set_normalize_labels_0_1(final_label):
    # labels_amounts diccionario de forma {label: cantidad_de_elementos}
    labels_amounts = Counter(final_label)
    label_least_amount = min(labels_amounts, key=labels_amounts.get)

    for label in labels_amounts:
        if label != 2:
            if label != label_least_amount:
                indexes_to_add = sample(
                    np.array(np.where(final_label == label)).tolist()[0], labels_amounts[label_least_amount]
                )
                list_of_indexes_to_add.append(indexes_to_add)
            else:
                indexes_to_add = np.array(np.where(final_label == label)).tolist()[0]
                list_of_indexes_to_add.append(indexes_to_add)
    return list_of_indexes_to_add


def normalize_input(final_label, total_input):
    # labels_amounts diccionario de forma {label: cantidad_de_elementos}
    labels_amounts = Counter(final_label)
    label_least_amount = min(labels_amounts, key=labels_amounts.get)

    # Creamos arrays llenos de ceros, con un tamaño máximo de: (cantidad para label con menos elementos) * (cantidad de labels)
    reduced_total_input = np.zeros(
        (labels_amounts[label_least_amount] * len(labels_amounts), 1, THRESHOLD, NUMBER_OF_FEATURES), dtype=float
    )
    reduced_final_label = np.zeros((labels_amounts[label_least_amount] * len(labels_amounts), 1), dtype=int)
    counter = 0

    # Se guardan los datos del label 0 y 1
    if len(list_of_indexes_to_add) == 0:
        set_normalize_labels_0_1(final_label)

    # Agregamos datos de etiqueta 0 y 1
    for indexes_to_add in list_of_indexes_to_add:
        for index in indexes_to_add:
            reduced_total_input[counter] = total_input[index]
            reduced_final_label[counter] = final_label[index]
            counter += 1

    # Agregamos datos de etiqueta 2
    indexes_to_add_2 = sample(np.array(np.where(final_label == 2)).tolist()[0], labels_amounts[label_least_amount])
    for index in indexes_to_add_2:
        reduced_total_input[counter] = total_input[index]
        reduced_final_label[counter] = final_label[index]
        counter += 1

    reduced_total_input = reduced_total_input[:counter, :, :, :]
    reduced_final_label = reduced_final_label[:counter, 0]
    return reduced_final_label, reduced_total_input


def get_epoch_data(final_label, total_input, NoClass):

    # Igualamos la cantidad de elementos para cada label
    final_label, total_input = normalize_input(final_label, total_input)

    # Making training and test data: 80% Training, 20% Test
    Train_X, Test_X, Train_Y, Test_Y_ori = train_test_split(total_input, final_label, test_size=0.20, random_state=7)

    Train_Y = tf.keras.utils.to_categorical(Train_Y, num_classes=NoClass)
    Test_Y = tf.keras.utils.to_categorical(Test_Y_ori, num_classes=NoClass)

    return Train_X, Train_Y, Test_X, Test_Y, Test_Y_ori


def main():
    tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(allow_soft_placement=True, log_device_placement=True))
    sess = print(tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(log_device_placement=True)))

    start_time = time.perf_counter()

    filename = "./outputs/Revised_KerasData_NoSmoothing.pickle"

    with open(filename, mode="rb") as f:
        total_input, final_label = pickle.load(f, encoding="latin1")  # Also can use the encoding 'iso-8859-1'

    NoClass = len(list(set(np.ndarray.flatten(final_label))))
    Threshold = len(total_input[0, 0, :, 0])

    # Making training and test data: 80% Training, 20% Test

    # Model and Compile
    # Sequential: modelo conformado por una secuencia de capas
    model = tf.keras.models.Sequential()
    activ = "relu"
    # Agregamos capas al modelo
    model.add(
        tf.keras.layers.Conv2D(
            32, (1, 3), strides=(1, 1), padding="same", activation=activ, input_shape=(1, Threshold, NUMBER_OF_FEATURES)
        )
    )
    model.add(tf.keras.layers.Conv2D(32, (1, 3), strides=(1, 1), padding="same", activation=activ))
    model.add(tf.keras.layers.MaxPooling2D(pool_size=(1, 2)))

    model.add(tf.keras.layers.Conv2D(64, (1, 3), strides=(1, 1), padding="same", activation=activ))
    model.add(tf.keras.layers.Conv2D(64, (1, 3), strides=(1, 1), padding="same", activation=activ))
    model.add(tf.keras.layers.MaxPooling2D(pool_size=(1, 2)))

    model.add(tf.keras.layers.Conv2D(128, (1, 3), strides=(1, 1), padding="same", activation=activ))
    model.add(tf.keras.layers.Conv2D(128, (1, 3), strides=(1, 1), padding="same", activation=activ))
    model.add(tf.keras.layers.MaxPooling2D(pool_size=(1, 2)))
    model.add(tf.keras.layers.Dropout(0.5))

    model.add(tf.keras.layers.Flatten())
    A = model.output_shape
    model.add(tf.keras.layers.Dense(int(A[1] * 1 / 4.0), activation=activ))
    model.add(tf.keras.layers.Dropout(0.5))

    # Capa de salida
    model.add(tf.keras.layers.Dense(NoClass, activation="softmax"))

    optimizer = tf.keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    # Compilamos el modelo, durante el entrenamiento pedimos que nos muestre accuracy
    model.compile(optimizer=optimizer, loss="categorical_crossentropy", metrics=["accuracy"])

    datetime_now = datetime.datetime.now()
    datetime_now = datetime_now.strftime("%Y_%m_%d_%H_%M_%S")
    filepath = "models/" + str(datetime_now) + "/model-{epoch:02d}-{val_accuracy:.2f}.hdf5"

    callbacks = [
        tf.keras.callbacks.ModelCheckpoint(
            "models/best_model.hdf5", monitor="val_accuracy", verbose=1, save_best_only=True, mode="max", period=1
        )
    ]

    # Entrenamos el modelo. Especificamos datos de entrenamiento y de test
    for i in range(EPOCHS):
        print("EPOCHS:", i)
        Train_X, Train_Y, Test_X, Test_Y, Test_Y_ori = get_epoch_data(final_label, total_input, NoClass)
        offline_history = model.fit(
            Train_X,
            Train_Y,
            epochs=1,
            batch_size=BATCH_SIZE,
            shuffle=False,
            validation_data=(Test_X, Test_Y),
            callbacks=[callbacks],
        )

    # Train_X, Train_Y, Test_X, Test_Y, Test_Y_ori = get_epoch_data(final_label, total_input, NoClass)
    # offline_history = model.fit(Train_X, Train_Y, epochs=30, batch_size=BATCH_SIZE, shuffle=False,
    #                                  validation_data=(Test_X, Test_Y),
    #                                  callbacks=[callbacks])

    # model.save_weights('models/model_weights.h5')
    # Devuelve un registro de las métricas pedidas
    hist = offline_history

    # Saving the test and training score for varying number of epochs.
    with open("./outputs/Revised_accuracy_history_largeEpoch_NoSmoothing.pickle", "wb") as f:
        pickle.dump([hist.epoch, hist.history["accuracy"], hist.history["val_accuracy"]], f)

    A = np.argmax(hist.history["val_accuracy"])
    print(
        "the optimal epoch size: {}, the value of high accuracy {}".format(
            hist.epoch[A], np.max(hist.history["val_accuracy"])
        )
    )
    best_model = tf.keras.models.load_model("models/best_model.hdf5")
    y_pred = np.argmax(best_model.predict(Test_X, batch_size=BATCH_SIZE), axis=1)
    print("Test Accuracy %: ", accuracy_score(Test_Y_ori, y_pred))
    print("\n")
    print("Confusion matrix: ", confusion_matrix(Test_Y_ori, y_pred))
    print("\n")
    print(classification_report(Test_Y_ori, y_pred, digits=3))


if __name__ == "__main__":
    main()
