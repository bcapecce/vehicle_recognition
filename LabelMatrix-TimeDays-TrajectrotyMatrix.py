import glob
import pickle
import re
import time
from datetime import datetime
from itertools import repeat
from multiprocessing import Pool, cpu_count
from pathlib import Path

import numpy as np


def date_to_days(time_str):
    """
    Convierte fecha de entrada a cantidad de días contando desde 1899/12/30.


    Parameters
    ----------
    time_str : str
        Fecha en formato %Y/%m/%d %H:%M:%S

    Returns
    -------
    float
        Días de diferencia entre fecha ingresada y 1899/12/30
    """
    current = datetime.strptime(time_str, "%Y/%m/%d %H:%M:%S")
    bench = datetime.strptime("1899/12/30", "%Y/%m/%d")
    delta = current - bench
    return delta.days + current.hour / 24.0 + current.minute / (24.0 * 60.0) + current.second / (24.0 * 3600.0)


def process_combined_file(combined_file_path):
    """
    Dado un archivo combinedN.plt, convierte sus datos en una matriz.


    Parameters
    ----------
    combined_file_path : str
        Path al archivo combinedN.plt

    Returns
    -------
    ndarray
        Matriz que contiene latitud, longitud y fecha en sus columnas
    """
    # Itera cada línea del archivo combinedN.plt y extrae la siguiente lista para cada una:
    # [Latitud, Longitud, No utilizado (0), Altitud, Cantidad de días desde 1899/12/30, Fecha, Hora]
    # Cada lista se agrega a table[]
    table = []
    try:
        with open(combined_file_path, "rb") as inp:
            for row in inp:
                row = row.rstrip()
                row = row.decode("utf-8")
                row = row.split(",")
                if len(row) == 7:
                    table.append(row)
    except IOError:
        print("error al abrir: " + combined_file_path)
        pass

    # Convierte table[] en un array de numpy, ejemplo para entender la transformación:
    # np.array([[1, 2], [3, 4]]) = array([[1, 2],
    #                                     [3, 4]])
    table_array = np.array(table, dtype=object)
    # trajectory_matrix = contains lat, long, date in each column
    trajectory_matrix = np.stack((table_array[:, 0], table_array[:, 1], table_array[:, 4]), axis=-1)
    for i in range(len(table_array[:, 0])):
        for j in range(3):
            trajectory_matrix[i, j] = float(trajectory_matrix[i, j])
    return trajectory_matrix


def process_labels_file(labels_file, chosen_labels, mode_index):
    """
    Dado un archivo labelsN.txt, convierte sus datos en una matriz.


    Parameters
    ----------
    labels_file : str
        Path al archivo labelsN.txt
    chosen_labels : list
        Lista de las etiquetas que se van a analizar
    mode_index : dict
        Diccionario que mapea etiquetas con sus índices

    Returns
    -------
    ndarray
        Array que contiene: timestamp de comienzo(en días), timestamp de finalización(days), y etiquetas
    """
    table = []

    # Itera cada línea del archivo labelsN.txt y extrae la siguiente lista para cada una:
    # [Start Time(%Y/%m/%d %H:%M:%S), End Time(%Y/%m/%d %H:%M:%S), Transportation Mode]
    # Cada lista se agrega a table[]
    try:
        with open(labels_file, "rb") as inp:
            for row in inp:
                row = row.rstrip()
                row = row.decode("utf-8")
                row = row.split("\t")
                if len(row) == 3:
                    table.append(row)
    except IOError:
        print("error al abrir: " + labels_file)
        pass

    # Convierte table[] en un array de numpy, ejemplo para entender la transformación:
    # np.array([[1, 2], [3, 4]]) = array([[1, 2],
    #                                     [3, 4]])
    label_array = np.array(table, dtype=object)

    start_time = []
    end_time = []
    label = []
    error = []

    for i in range(len(label_array[:, 0])):
        try:
            # Verifica que las etiquetas del array se encuentren dentro de chosen_labels
            if label_array[i, 2] in chosen_labels:
                # Cambia el formato de start_time y end_time (%Y/%m/%d %H:%M:%S) a 'cantidad de días', acumula los resultados en respectivos arrays
                start_time.append(date_to_days(label_array[i, 0]))
                end_time.append(date_to_days(label_array[i, 1]))
                # Traduce las etiquetas de string al índice indicado en mode_index, guarda la conversión en array
                label.append(mode_index[label_array[i, 2]])
        except ValueError:
            error.append(i)
    # Junta los arrays filtrados
    label_matrix = (np.vstack((start_time, end_time, label))).T
    return label_matrix


def assign_labels_to_trajectories(trajectory_matrix, label_matrix):
    """
    Combina la matriz de trayectorias con la de labels


    Parameters
    ----------
    trajectory_matrix : ndarray
        Matriz que contiene latitud, longitud y fecha en sus columnas
    label_matrix : ndarray
        Array que contiene: timestamp de comienzo(en días), timestamp de finalización(days), y etiquetas

    Returns
    -------
    ndarray
        Matriz que contiene latitud, longitud, timestamp, índice de label
    """
    # Assign the labels to the trajectories
    # Trajectory = zip(lat, long, date)
    dates = np.split(trajectory_matrix, 3, axis=-1)[2]
    sec = 1 / (24.0 * 3600.0)
    # c_list: all the rows in the trajectory_matrix that should be picked up
    c_list = []
    # mode_trajectory: all labels
    mode_trajectory = []
    for index, row in enumerate(label_matrix):
        a = np.where(dates >= (float(row[0]) - sec))
        b = np.where(dates <= (float(row[1]) + sec))
        c = list(set(a[0]).intersection(b[0]))
        # if len(c) == 0:
        #     print("error")

        [mode_trajectory.append(row[2]) for i in c]
        [c_list.append(i) for i in c]

    trajectory_matrix = [trajectory_matrix[i, :] for i in c_list]
    trajectory_matrix = np.array(trajectory_matrix)
    mode_trajectory = np.array(mode_trajectory)
    trajectory_label = (np.vstack((trajectory_matrix.T, mode_trajectory))).T
    # trajectory_label tiene la forma: [[lat,long,timestamp,índice de label]
    #                                   [lat,long,timestamp,índice de label]
    #                                   ...
    #                                   [lat,long,timestamp,índice de label]]
    return trajectory_label


def process(sorted_combined_files_paths, sorted_label_files_paths, chosen_labels, mode_index, pool_index):
    # trajectory_array and label_array are the final lists which each of its element is for one user
    # Cada elemento de trajectory_array corresponde a un usuario y contiene una matriz con: lat, long, date
    trajectory_array = []
    # Cada elemento de label_array corresponde a un usuario y contiene
    label_array = []
    trajectory_label_array = []

    for i in range(len(sorted_combined_files_paths)):
        trajectory_matrix = process_combined_file(sorted_combined_files_paths[i])

        trajectory_array.append(trajectory_matrix)

        label_matrix = process_labels_file(sorted_label_files_paths[i], chosen_labels, mode_index)

        label_array.append(label_matrix)

        trajectory_label_array.append(assign_labels_to_trajectories(trajectory_matrix, label_matrix))
    return (pool_index, trajectory_label_array)


def split_workload(folder_path, files_paths, cpu_cores):
    folder_size = sum(f.stat().st_size for f in Path(folder_path).glob("**/*") if f.is_file())
    max_slice_size = int(folder_size / cpu_cores)
    plt_slices = [[] for _ in range(cpu_cores)]
    label_slices = [[] for _ in range(cpu_cores)]
    current_slice_size = 0
    current_slice = 0
    for plt_file_path in files_paths:
        # numero dentro del nombre del archivo, para encontrar el labels.txt correspondiente
        combined_file_number = re.findall(r"\d+", plt_file_path)[0]
        label_file_path = folder_path + "labels" + combined_file_number + ".txt"
        total_size = Path(plt_file_path).stat().st_size + Path(label_file_path).stat().st_size
        if total_size + current_slice_size < max_slice_size or current_slice == cpu_cores - 1:
            plt_slices[current_slice].append(plt_file_path)
            label_slices[current_slice].append(label_file_path)
            current_slice_size += total_size
        else:
            plt_slices[current_slice + 1].append(plt_file_path)
            label_slices[current_slice + 1].append(label_file_path)
            current_slice_size = total_size
            current_slice += 1
    return plt_slices, label_slices


def main():
    start_time = time.perf_counter()
    dataset_folder_path = "./customized-geolife-dataset/"

    # Mode index original
    # mode_index = {
    #     "walk": 0,
    #     "run": 2,
    #     "bike": 2,
    #     "bus": 2,
    #     "car": 1,
    #     "taxi": 2,
    #     "subway": 2,
    #     "railway": 2,
    #     "train": 2,
    #     "motocycle": 2,
    #     "boat": 2,
    #     "airplane": 2,
    #     "other": 2,
    # }

    # Asignamos índices a las etiquetas de la columna "Mode"
    mode_index = {
        "walk": 0,
        "run": 1,
        "bike": 2,
        "bus": 3,
        "car": 4,
        "taxi": 5,
        "subway": 6,
        "railway": 7,
        "train": 8,
        "motocycle": 9,
        "boat": 10,
        "airplane": 11,
        "other": 12,
    }

    # Lista de los tipos de etiquetas que vamos a analizar
    chosen_labels = [
        "walk",
        "run",
        "bike",
        "bus",
        "car",
        "taxi",
        "subway",
        "railway",
        "train",
        "motocycle",
        "boat",
        "airplane",
        "other",
    ]

    # Lista los archivos .plt en dataset_folder_path y los ordena según el nro en el nombre
    # (Por algún motivo, el resultado es correcto sólo cuando está ordenado)
    sorted_combined_files_paths = sorted(
        glob.iglob(dataset_folder_path + "/*.plt"), key=lambda x: int(re.findall(r"\d+", x)[0])
    )

    trajectory_label_array = []

    cores = cpu_count()
    print(f"Utilizando {cores} cores")

    # Divide el array a procesar según cantidad de cores
    split_sorted_combined_files_paths, split_sorted_label_files_paths = split_workload(
        dataset_folder_path, sorted_combined_files_paths, cores
    )

    # Inicia un pool con la cantidad de procesadores que devuelva os.cpu_count()
    pool = Pool()

    # Pasamos parte del input a cada proceso, y un índice para ordenar los resultados al terminar

    results = pool.starmap(
        process,
        zip(
            split_sorted_combined_files_paths,
            split_sorted_label_files_paths,
            repeat(chosen_labels),
            repeat(mode_index),
            range(0, cores),
        ),
    )
    pool.close()
    pool.join()
    # Ordenamos los resultados según pool_index
    results.sort(key=lambda x: x[0])
    for r in results:
        index, res = r
        trajectory_label_array.extend(res)

    # Save trajectory_array and label_array for all users
    with open("./outputs/Revised_Trajectory_Label_Array.pickle", "wb") as f:
        pickle.dump(trajectory_label_array, f)
    print("Running time", time.perf_counter() - start_time)


if __name__ == "__main__":
    main()
